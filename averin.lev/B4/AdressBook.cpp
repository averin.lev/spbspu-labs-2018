#include "AddressBook.hpp"
#include <string>
#include <iostream>

AddressBook::iterator AddressBook::add(recordType record)
{
  addressList_.push_back(record);
  if(addressList_.size() != 1)
  {
    currentRecord_ = std::prev(addressList_.end());
  }
  else
  {
    currentRecord_ = addressList_.begin();
  }
  return currentRecord_;
}

AddressBook::iterator AddressBook::erase(iterator & position)
{
  return addressList_.erase(position);
}

AddressBook::iterator AddressBook::addAfter(AddressBook::iterator position, recordType record)
{
  position = addressList_.insert(std::next(position), record);
  return position;
}

void AddressBook::replace(recordType record)
{
  if(currentRecord_ != addressList_.end())
  {
    currentRecord_ = addressList_.erase(currentRecord_++);
    currentRecord_ = addressList_.insert(currentRecord_, record);
  }
}

AddressBook::iterator AddressBook::addBefore(AddressBook::iterator position, recordType record)
{
  position = addressList_.insert(position, record);    
  return position;
}

void AddressBook::goForward()
{
  if(std::next(currentRecord_) != addressList_.end())
  {
    ++currentRecord_;
  }
}

void AddressBook::goBack()
{
  if(currentRecord_ != addressList_.begin())
  {
    --currentRecord_;
  }
}

void AddressBook::show() const
{
  if(currentRecord_ != addressList_.end())
  {
    std::cout << currentRecord_->first << " " << currentRecord_->second << std::endl;
  }
}

bool AddressBook::empty() const
{
  return addressList_.empty();
}

AddressBook::iterator AddressBook::begin()
{
  return addressList_.begin();
}

AddressBook::iterator AddressBook::end()
{
  return addressList_.end();
}
