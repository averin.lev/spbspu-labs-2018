#ifndef ADDRESS_BOOK_HPP
#define ADDRESS_BOOK_HPP

#include <string>
#include <memory>
#include <list>

class AddressBook
{
public:
  typedef std::pair<std::string, std::string> recordType;
  typedef std::list<recordType> addressListType; 
  typedef addressListType::iterator iterator;
  typedef addressListType::difference_type difference_type;
  
  AddressBook() : addressList_()
  {
    currentRecord_ = addressList_.end();
  }
  iterator add(recordType record);
  iterator addAfter(iterator position, recordType record);
  iterator addBefore(iterator position, recordType record);
  void replace(recordType record);
  void goBack();
  void goFor(iterator & it, addressListType::difference_type stepNumber);
  void goForward();
  iterator erase(iterator & position);
  void show() const;
  bool empty() const;
  iterator begin();
  iterator end();
  
private:
   addressListType addressList_;
   iterator currentRecord_;
};

#endif
