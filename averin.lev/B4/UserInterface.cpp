#include "UserInterface.hpp"
#include "AddressBook.hpp"
#include <iostream>
#include <sstream>
#include <algorithm>

std::istream& operator>>(std::istream & stream, AddressBook::recordType & record)
{
  const char BOUND_CHAR = '\"';

  std::istream::sentry sentry(stream);
  if(sentry)
  {
    stream >> std::ws >> record.first;
    char c;
    stream >> c;
    if(stream.fail())
    {
      return stream;
    }
    if(c != BOUND_CHAR)
    {
      stream.setstate(std::istream::failbit);
      return stream;
    }
    std::string tmp;
    std::getline(stream, tmp); 
    tmp = tmp.substr(0, tmp.rfind(BOUND_CHAR));
    tmp.erase(std::remove(tmp.begin(),tmp.end(),'\\'),tmp.end()); 
    record.second = tmp;  
  }
  return stream;
}

UserInterface::UserInterface() : book_()
{
  bookmarks_.emplace("current", book_.end());
}

void UserInterface::readCommand()
{
  const std::string INVALID_COMMAND = "<INVALID COMMAND>";

  while(std::cin)
  {
    std::string input;
    std::getline(std::cin, input);
    std::istringstream line(input);
    
    if(std::cin.fail())
    {
      if(std::cin.eof())
      {
        break;
      }
      else
      {
        throw std::invalid_argument("Invalid input.");
      }
    }
    else
    {
      std::string inputWord, tmp;
      line >> inputWord;
      if(inputWord == "add")
      {
        AddressBook::recordType record;
        line >> record;
        if(!line.fail())
        {
          add(record);
        }
        else
        {
          std::cout << INVALID_COMMAND << std::endl;
        }
      }
      else if(inputWord == "store")
      {
        std::string markName, newMarkName;
        line >> markName;
        line >> newMarkName;
        storeBookmark(markName, newMarkName);
      }
      else if(inputWord == "insert")
      {
        std::string markName;
        line >> tmp;
        line >> markName;
        AddressBook::recordType record;
        line >> record; 
        
        if(!line.fail())
        {
          if(tmp == "before")
          {
            insertBefore(markName, record);
          }
          else if(tmp == "after")
          {
            insertAfter(markName, record);
          }
        }
        else
        {
          std::cout << INVALID_COMMAND << std::endl;
        }
      }
      else if(inputWord == "delete")
      {
        line >> tmp;
        erase(tmp);
      }
      else if(inputWord == "show")
      {
        line >> tmp;
        show(tmp);
      }
      else if(inputWord == "move")
      {
        line >> tmp;
        std::string keyWord;
        line >> keyWord;
        if(keyWord == "first")
        {
          moveFirst(tmp);
        }
        else if(keyWord == "last")
        {
          moveLast(tmp);
        }
        else
        {
          std::istringstream stream(keyWord);
          AddressBook::difference_type steps;
          stream >> steps;
          
          if(stream.fail())
          {
            std::cout << "<INVALID STEP>" << std::endl;
          }
          move(tmp,steps);
        }
      }
      else
      {
        std::cout << INVALID_COMMAND << std::endl;
      }
    }
  }
}

void UserInterface::show(std::string markName)
{
  UserInterface::iterator tmp = bookmarks_.find(markName);
  if(tmp != bookmarks_.end())
  {
    if(tmp->second != book_.end())
    {
      std::cout << tmp->second->first << " " << tmp->second->second << std::endl;
    }
    else
    {
      std::cout << "<EMPTY>" << std::endl;
    }
  }
  else
  {
    std::cout << "<INVALID BOOKMARK>" << std::endl;
  }
}

void UserInterface::storeBookmark(std::string markName, std::string newMarkName)
{
  UserInterface::iterator it = bookmarks_.find(markName);
  if(it != bookmarks_.end())
  {
    if(!(bookmarks_.emplace(newMarkName, it->second).second))
    {
      bookmarks_[newMarkName] = it->second;
    }
  }
  else
  {
    std::cout << "<INVALID BOOKMARK>" << std::endl;
  }
}

void UserInterface::addWithBookmark(std::string markName, std::string number, std::string name)
{
  AddressBook::recordType record = std::make_pair(number,name);
  AddressBook::iterator it = book_.add(record);
  bookmarks_.emplace(markName, it);
}

void UserInterface::insertBefore(std::string markName, AddressBook::recordType record)
{
  UserInterface::iterator position = bookmarks_.find(markName);
  if(position != bookmarks_.end())
  {
    AddressBook::iterator it = book_.addBefore(position->second, record);
    if(bookmarks_.size() == 1)
    {
      position->second = it;
    }
  }
  else
  {
    std::cout << "<INVALID BOOKMARK>" << std::endl;
  }
}

void UserInterface::insertAfter(std::string markName, AddressBook::recordType record)
{
  UserInterface::iterator position = bookmarks_.find(markName);
  if(position != bookmarks_.end())
  {
    AddressBook::iterator it = book_.addAfter(position->second, record);
    if(bookmarks_.size() == 1)
    {
      position->second = it;
    }
  }
  else
  {
    std::cout << "<INVALID BOOKMARK>" << std::endl;
  }
}

void UserInterface::add(AddressBook::recordType record)
{  
  if(book_.empty())
  {
    AddressBook::iterator it = book_.add(record);
    UserInterface::iterator currentPosition = bookmarks_.find("current");
    currentPosition->second = it;
  }
  else
  {
    book_.add(record);
  }
}

void UserInterface::moveFirst(std::string markName)
{
  UserInterface::iterator position = bookmarks_.find(markName);
  position->second = book_.begin();
}

void UserInterface::moveLast(std::string markName)
{
  UserInterface::iterator position = bookmarks_.find(markName);
  position->second = std::prev(book_.end());
}

void UserInterface::move(std::string markName, AddressBook::difference_type stepNumber)
{
  UserInterface::iterator position = bookmarks_.find(markName);
  
  if(position != bookmarks_.end())
  {  
    for(AddressBook::difference_type count = 0; count < std::abs(stepNumber); ++count)
    {
      (stepNumber > 0) ? ++position->second : --position->second;  
    }
  }
}

void UserInterface::erase(std::string markName)
{
  if(!book_.empty())
  {
    UserInterface::iterator position = bookmarks_.find(markName);
    AddressBook::iterator oldIt = position->second;
    position->second = book_.erase(position->second);
    if(position->second == book_.end())
    {
      std::advance(position->second,-1);
    }
    for(UserInterface::iterator it = bookmarks_.begin(); it != bookmarks_.end(); ++it)
    {
      if(it->second == oldIt)
      {
        it->second = position->second;
      }
    }
  }
}
