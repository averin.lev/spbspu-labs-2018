#ifndef USER_INTERFACE_HPP
#define USER_INTERFACE_HPP

#include "AddressBook.hpp"
#include <vector>
#include <map>
#include <iosfwd>

std::istream& operator>>(std::istream & stream, AddressBook::recordType & record);

class UserInterface
{
public:
  typedef std::pair<std::string, AddressBook::iterator> bookmarkType;
  typedef std::map<std::string, AddressBook::iterator> bookmarksListType; 
  typedef bookmarksListType::iterator iterator;

  UserInterface();
  void readCommand();
  
  void show(std::string markName);
  void add(AddressBook::recordType record);
  void addWithBookmark(std::string markName, std::string number, std::string name);
  void storeBookmark(std::string markName, std::string newMarkName);
  void insertBefore(std::string markName, AddressBook::recordType record);
  void insertAfter(std::string markName, AddressBook::recordType record);
  void move(std::string markName, AddressBook::difference_type stepNumber);
  void moveFirst(std::string markName);
  void moveLast(std::string markName);
  void erase(std::string markName);  
  
  private:
  bookmarksListType bookmarks_;
  AddressBook book_;
};

#endif
