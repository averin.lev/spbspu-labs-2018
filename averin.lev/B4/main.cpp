#include "UserInterface.hpp"
#include "FactorialContainer.hpp"
#include <iostream>
#include <sstream>
#include <iterator>

int main(int argc, char *argv[])
{
  try
  {
    if (argc != 2)
    {
      throw std::invalid_argument("Invalid number of arguments");
    }
    int num = 0;
    if (std::atoi(argv[1]))
    {
      num = std::atoi(argv[1]);
    }
    else
    {
      throw std::invalid_argument("Invalid argument");
    }
  
    switch(num)
    {
      case 1:
      {
        UserInterface userInterface;
        userInterface.readCommand();
        break;
      }
      case 2:
      {
        FactorialContainer factorialContainer;
        std::copy(factorialContainer.begin(), factorialContainer.end(), 
            std::ostream_iterator<int>(std::cout," "));
        std::cout << "\n";
                
        std::copy(factorialContainer.rbegin(), factorialContainer.rend(), 
            std::ostream_iterator<int>(std::cout," "));
        std::cout << "\n";
        break;
      }
      default:
      {
        throw std::invalid_argument("Invalid argument");
      }
    }  
  }
  catch(const std::invalid_argument& e)
  {
    std::cout << e.what() << std::endl;
    return 1;
  }
  return 0;
}
