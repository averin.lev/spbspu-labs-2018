#include <list>
#include <iterator>

#include "declarations.hpp"

void doSecondTask()
{
  std::list<int> list = {std::istream_iterator<int>(std::cin), std::istream_iterator<int>()};

  if (!std::cin.eof() && std::cin.fail())
  {
    throw std::invalid_argument("Invalid input.");
  }

  if (list.size() > 20)
  {
    throw std::invalid_argument("List size > 20");
  }
  if (list.size() == 0)
  {
    exit(0);
  }

  for (auto iter = list.begin(); iter != list.end(); iter++)
  {
    if (*iter > 20 || *iter < 1)
    {
      throw std::invalid_argument("Out of range");
    }
  }

  auto iter1 = list.begin();

  if (list.size() == 1)
  {
    std::cout << *iter1 << "\n";
    return;
  }

  auto iter2 = list.end();
  --iter2;

  while (iter1 != iter2)
  {
    std::cout << *iter1 << " " << *iter2 << " ";
    ++iter1;
    if (iter1 == iter2)
    {
      break;
    }
    --iter2;
  }

  if (list.size() % 2 == 1)
  {
    std::cout << *iter1;
  }
  std::cout << "\n";
}
