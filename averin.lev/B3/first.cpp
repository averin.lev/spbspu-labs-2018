#include <iostream>
#include <sstream>
#include "declarations.hpp"

#include "QueueWithPriority.hpp"

void doFirstTask()
{
  typedef QueueWithPriority<std::string> str_queue;
  str_queue QWP;
  std::string line;
  std::skipws(std::cin);
  while (std::getline(std::cin,line))
  {
    std::stringstream ss(line);
    std::string cmd,test;
    ss >> cmd;

    if (cmd == "add")
    {
      std::string priority;
      ss >> priority;

      std::string element;
      ss.ignore(1);
      std::getline(ss,element);

      if (element.empty())
      {
        std::cout << "<INVALID COMMAND>" << "\n";
      }
      else if (priority == "high")
      {
        QWP.putElementToQueue(element,ElementPriority::HIGH);
      }
      else if (priority == "normal")
      {
        QWP.putElementToQueue(element,ElementPriority::NORMAL);
      }
      else if (priority == "low")
      {
        QWP.putElementToQueue(element,ElementPriority::LOW);
      }
      else
      {
        std::cout << "<INVALID COMMAND>" << "\n";
      }
    }
    else
    {
      ss >> test;
      if (test.empty())
      {
        if ((cmd == "get") && (ss.eof()))
        {
          if (QWP.isEmpty())
          {
            std::cout << "<EMPTY>" << "\n";
          }
          else
          {
            std::cout << QWP.getElementFromQueue() << "\n";
          }
        }
        else if ((cmd == "accelerate") && (ss.eof()))
        {
          QWP.accelerate();
        }
        else
        {
          std::cout << "<INVALID COMMAND>" << "\n";
        }
      }
      else
      {
        std::cout << "<INVALID COMMAND>" << "\n";
      }
    }

    if (std::cin.eof())
    {
      break;
    }
  }
}
