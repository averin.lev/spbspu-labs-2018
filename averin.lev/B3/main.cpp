#include "QueueWithPriority.hpp"
#include "declarations.hpp"

int main(int argc, char** argv) 
{
  try
  {
    if (argc != 2)
    {
      throw std::invalid_argument("Invalid number of arguments");
    }
    int num = 0;
    if (std::atoi(argv[1]))
    {
      num = std::atoi(argv[1]);
    }
    else
    {
      throw std::invalid_argument("Invalid argument");
    }
    switch (num)
    {
    case 1:
    {
      doFirstTask();
      break;
    }
    case 2:
    {
      doSecondTask();
      break;
    }
    default:
    {
      throw std::invalid_argument("Invalid argument");
    }
    }      
  }
  catch(std::exception& e)
  {
    std::cout << e.what() << std::endl;
    return 1;
  }
  return 0;
}
