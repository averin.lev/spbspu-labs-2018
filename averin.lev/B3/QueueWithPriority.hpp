#ifndef QUEUEWITHPRIORITY_HPP
#define QUEUEWITHPRIORITY_HPP
#include <string>
#include <iostream>
#include <list>

typedef enum
{
  LOW,
  NORMAL,
  HIGH
} ElementPriority;

typedef struct
{
  std::string name;
} QueueElement;

template <typename T>

class QueueWithPriority
{
public:

  ~QueueWithPriority() = default;

  void putElementToQueue(const T& element, ElementPriority priority)
  {
    queue_[priority].push_back(element);
  }
  
  T getElementFromQueue()
  {
    if (queue_[2].size() != 0)
    {
      T temp = *queue_[2].begin();
      queue_[2].pop_front();
      return temp;
    }

    if (queue_[1].size() != 0)
    {
      T temp = *queue_[1].begin();
      queue_[1].pop_front();
      return temp;
    }

    if (queue_[0].size() != 0)
    {
      T temp = *queue_[0].begin();
      queue_[0].pop_front();
      return temp;
    }
    return "<EMPTY>";
  }

  void accelerate()
  {
    queue_[ElementPriority::HIGH].splice(queue_[ElementPriority::HIGH].end(),queue_[ElementPriority::LOW]);
  }
  
  bool isEmpty()
  {
    if (queue_[0].empty() && queue_[1].empty() && queue_[2].empty())
    {
      return true;
    }
    return false;
  }
  
private:
  
  std::list<T> queue_[3];
};



#endif 
