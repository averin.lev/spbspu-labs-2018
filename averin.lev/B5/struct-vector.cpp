#include "struct-vector.hpp"

void fillVector(std::vector<DataStruct>& vector)
{
  std::skipws(std::cin);
  std::string line;
  std::string tmp;
  char c = ' ';
  DataStruct ds;
  while (std::getline(std::cin,line))
  {
    std::stringstream ss(line);
    ss >> ds.key1;
    if (ss.fail() || ds.key1 > 5 || ds.key1 < -5)
    {
      throw std::invalid_argument("Invalid input");
    }
    ss >> c;
    if (c != ',')
    {
      std::cout << c;
      throw std::invalid_argument("Invalid input");
    }
    ss >> ds.key2;
    if (ss.fail() || ds.key2 > 5 || ds.key2 < -5)
    {
      throw std::invalid_argument("Invalid input");
    }
    ss >> c;
    if (c != ',')
    {
      throw std::invalid_argument("Invalid input");
    }
    while (!ss.eof())
    {
      ss >> tmp;
      ds.str += tmp;
      ds.str += " ";
    }
    ds.str.pop_back();
    vector.push_back(ds);
    ds.str.clear();
  }
}

void sortVector(std::vector<DataStruct>& vector)
{
  std::sort(vector.begin(), vector.end(), 
    [] (const DataStruct& ds1, const DataStruct& ds2)
    {
      if (ds1.key1 != ds2.key1)
      {
        return (ds1.key1 < ds2.key1);
      }
      if (ds1.key2 != ds2.key2)
      {
        return (ds1.key2 < ds2.key2);
      }
      return (ds1.str.length() < ds2.str.length());
    }
  );
}

void printVector(std::vector<DataStruct> vector)
{
  for (auto iter = vector.begin(); iter != vector.end(); iter++)
  {
    std::cout << iter->key1 << "," << iter->key2 << "," << iter->str << std::endl;
  }
}
