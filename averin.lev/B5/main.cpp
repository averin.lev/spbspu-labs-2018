#include "struct-vector.hpp"


int main() 
{
  std::vector<DataStruct> vector;
  try
  {
    fillVector(vector);
    sortVector(vector);
    printVector(vector);
  }
  catch(std::exception& e)
  {
    std::cout << e.what() << std::endl;
    return 1;
  }
  return 0;
}

