#ifndef STRUCT_VECTOR_HPP
#define STRUCT_VECTOR_HPP
#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>

struct DataStruct
{
  int key1;
  int key2;
  std::string str;
};

void fillVector(std::vector<DataStruct>& vector);

void sortVector(std::vector<DataStruct>& vector);

void printVector(std::vector<DataStruct> vector);


#endif

