#include "declarations.hpp"
#include <fstream>
#include <memory>

void doSecondTask(char* filename)
{
  std::ifstream file(filename);
  if (!file.is_open())
  {
    throw std::invalid_argument("Unable to open file");
  }
  if (!file)
  {
    throw std::invalid_argument("Unable to read file");
  }
  file.seekg(0, std::ios_base::end);
  long long int length = file.tellg();
  file.seekg(0);
  if (length == 0)
  {
    exit(0);
  }
  std::unique_ptr<char[]> buffer(new char[length]);
  file.read(buffer.get(),length);
  file.close();
  if (file.is_open())
  {
    throw std::invalid_argument("Unable to close file");
  }
  std::vector<char> vec(buffer.get(),buffer.get() + length);
  for (long long int i = 0; i < length; i++)
  {
    std::cout << vec[i];
  }
}
