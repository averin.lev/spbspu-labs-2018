#include "declarations.hpp"


void detail::pushTree(Tree* t,double num)
{
  if (num > t->data && t->right == nullptr)
  {
    t->right = new Tree;
    t->right->data = num;
    t->right->left = nullptr;
    t->right->right = nullptr;
    return;
  }
  if (num > t->data && t->right !=nullptr)
  {
    pushTree(t->right,num);
    return;
  }
  if (num <= t->data && t->left == nullptr)
  {
    t->left = new Tree;
    t->left->data = num;
    t->left->right = nullptr;
    t->left->left = nullptr;
    return;
  }
  if (num <= t->data && t->left != nullptr)
  {
    pushTree(t->left,num);
    return;
  }
}

void detail::copyTree(Tree* t, std::vector<double>& v, bool asc)
{
  if (asc)
  {
    if (t->left != nullptr)
    {
      copyTree(t->left,v,asc);
    }
    v.push_back(t->data);
    if (t->right != nullptr)
    {
      copyTree(t->right,v,asc);
    }
  }
  
  if (!asc)
  {
    if (t->right != nullptr)
    {
      copyTree(t->right,v,asc);
    }
    v.push_back(t->data); 
    if (t->left != nullptr)
    {
      copyTree(t->left,v,asc);
    }
  }
  t->left = nullptr;   
  t->right = nullptr;
  delete t;
}
