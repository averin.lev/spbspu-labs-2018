#ifndef DECLARATIONS_HPP
#define DECLARATIONS_HPP

#include <vector>
#include <iostream>
#include <forward_list>
#include <string.h>

namespace detail 
{
  template <typename T> 

  struct BrAccess
  {
    static typename T::reference getElement(T& collection, unsigned int index)
    {
      return collection[index];
    }
    static int begin(T)
    {
      return 0;
    }
    static int end(T collection)
    {
      return collection.size();
    }
  };

  template <typename T>

  struct AtAccess 
  {
    static typename T::reference getElement(T& collection, unsigned int index)
    {
      return collection.at(index);
    }
    static int begin(T)
    {
      return 0;
    }
    static int end(T collection)
    {
      return collection.size();
    }
  };

  template <typename T> 

  struct ItAccess
  {
    static typename T::reference getElement(T&, typename T::iterator iter)
    {
      return *iter;
    }
    static typename T::iterator begin(T& collection)
    {
      return collection.begin();
    }
    static typename T::iterator end(T& collection)
    {
      return collection.end();
    }
  };
  
  struct Tree
  {
    double data;
    Tree *left, *right;
  };
  
  void copyTree(Tree* t, std::vector<double>& v, bool asc);

  void pushTree(Tree* t, double num);
  
}

template<template <typename> class getAccess, class T>

void sort(T& collection, char* direction)
{
  auto begin = getAccess<T>::begin(collection);
  auto end = getAccess<T>::end(collection);
  detail::Tree *t = new detail::Tree;
  if (begin == end)
  {
    exit(0);
  }
  t->data = getAccess<T>::getElement(collection,begin);
  t->left = nullptr;
  t->right = nullptr;
  for (auto iter = ++begin; iter != end; iter++)
  {
    detail::pushTree(t,getAccess<T>::getElement(collection,iter));
  }
  std::vector<double> v;
  bool asc = 0;
  if (!strcmp(direction,"ascending"))
  {
    asc = 1;
  }
  else
  {
    asc = 0;
  }
  detail::copyTree(t,v,asc);
  collection.assign(v.begin(),v.end());
}

template <typename T>

void printCollection(T collection)
{
  auto iter = collection.begin();
  while (iter != collection.end())
  {
    std::cout << *iter << " ";
    iter++;
  }
  std::cout << std::endl;
}

void doFirstTask(char* direction);

void doSecondTask(char* filename);

void doThirdTask();

void doFourthTask(char* direction, long long int size);

#endif
