#include "declarations.hpp"

void doThirdTask()
{
  std::vector<int> vector;
  int num = 0;
  bool chck = 0;
  while (std::cin >> num)
  {
    if (num == 0)
    {
      chck = 1;
      break;
    }   
    vector.push_back(num);
  }
  if (std::cin.fail() && !std::cin.eof())
  {
    throw std::invalid_argument("Not a integer value");
  }
  if (vector.empty())
  {
    exit(0);
  }
  if (!chck)
  {
    throw std::invalid_argument("No zero");
  }

  if (*--vector.end() == 1)
  {
    for (auto iter = vector.begin(); iter != vector.end(); )
    {
      if (*iter % 2 == 0)
      {
        iter = vector.erase(iter);
      }
      else
      {
        iter++;
      }
    }
  }
  if (*--vector.end() == 2)
  {
    for (auto iter = vector.begin(); iter != vector.end(); )
    {
      if (*iter % 3 == 0)
      {
        for (int i = 0; i < 3; i++)
        {
          iter = vector.insert(iter + 1, 1);
        }
      }
      else
      {
        iter++;
      }
    }
  }
  printCollection(vector);
}
