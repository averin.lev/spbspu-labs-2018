#ifndef GETSTATS_HPP
#define GETSTATS_HPP
#include <algorithm>

class GetStats
{
public:
  GetStats();
  
  void operator()(long long int x);
  
  long long int getMax();
  
  long long int getMin();
  
  double getAverage();
  
  long long int getPositive();
  
  long long int getNegative();
  
  long long int getOddSum();
  
  long long int getEvenSum();
  
  bool isFirstEqualLast();
  
  long long int getNum();
  
private:
  
  long long int max_;
  
  long long int min_;
  
  double average_;
  
  long long int positive_;
  
  long long int negative_;
  
  long long int odd_;
  
  long long int even_;
  
  double first_;
  
  double last_;
  
  double num_;
  
};


#endif

