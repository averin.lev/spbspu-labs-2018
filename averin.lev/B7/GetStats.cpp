#include "GetStats.hpp"

GetStats::GetStats():
  max_(0),
  min_(0),
  average_(0),
  positive_(0),
  negative_(0),
  odd_(0),
  even_(0),
  first_(0),
  last_(0),
  num_(0)
{
  
}

void GetStats::operator ()(long long int x)
{
  if (num_ == 0)
  {
    first_ = x;
    max_ = x;
    min_ = x;
  }
  last_ = x;
  max_ = std::max(max_, x);
  min_ = std::min(min_, x);
  if (x > 0)
  {
    positive_++;
  }
  if (x < 0)
  {
    negative_++;
  }
  if (x % 2 == 0)
  {
    even_ += x;
  }
  else
  {
    odd_ += x;
  }
  num_++;
}

long long int GetStats::getMax()
{
  return max_;
}

long long int GetStats::getMin()
{
  return min_;
}

double GetStats::getAverage()
{
  average_ = (getOddSum() + getEvenSum()) / num_;
  return average_;
}

long long int GetStats::getPositive()
{
  return positive_;
}

long long int GetStats::getNegative()
{
  return negative_;
}

long long int GetStats::getOddSum()
{
  return odd_;
}
        
long long int GetStats::getEvenSum()
{
  return even_;
}

bool GetStats::isFirstEqualLast()
{
  if (first_ == last_)
  {
    return true;
  }
  return false;
}

long long int GetStats::getNum()
{
  return num_;
}
