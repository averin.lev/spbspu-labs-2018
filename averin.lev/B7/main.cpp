#include "GetStats.hpp"
#include <iterator>
#include <iostream>
#include <iomanip>

int main() 
{
  GetStats stats = std::for_each((std::istream_iterator<long long int>(std::cin)), std::istream_iterator<long long int>(), GetStats());
  try
  {
    if (!std::cin.eof())
    {
      throw std::invalid_argument("Invalid input");
    }
    if (!stats.getNum())
    {
      std::cout << "No Data" << std::endl;
      return 0;
    }
    std::cout << "Max: " << stats.getMax() << std::endl;
    std::cout << "Min: " << stats.getMin() << std::endl;
    std::cout << "Mean: " << std::fixed << std::setprecision(1) << stats.getAverage() << std::endl;
    std::cout << "Positive: " << stats.getPositive() << std::endl;
    std::cout << "Negative: " << stats.getNegative() << std::endl;
    std::cout << "Odd Sum: " << stats.getOddSum() << std::endl;
    std::cout << "Even Sum: " << stats.getEvenSum() << std::endl;
    std::cout << "First/Last Equal: " << (stats.isFirstEqualLast() ? "yes" : "no")<< std::endl;
  }
  catch (std::exception& e)
  {
    std::cout << e.what() << std::endl;
    return 1;
  }
  return 0;
}

