#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
#include <locale>

#include "TextReader.hpp"

TextReader::TextReader(std::istream& in):
  in_(in),
  loc_(in.getloc()),
  last_symb_(none)
{

}

char TextReader::readChar()
{
  char c = '\0';
  if (in_)
  {
    in_.get(c);
  }
  return c;
}

std::string TextReader::readWord()
{
  std::string str;
  char c = readChar();
  int length = 0;
  do {
    str.push_back(c);
    ++length;
    if ((c == '-') && (in_.peek() == '-'))
    {
      break;
    }
    c = readChar();
  } while ((std::isalpha(c,loc_)) || (c == '-'));
  in_.unget();
  if (length > 20)
  {
    throw std::invalid_argument("Invalid text");
  }
  return str;
}

std::string TextReader::readNum()
{
  std::string str;
  char c = readChar();
  char dec_point = std::use_facet<std::numpunct<char> >(loc_).decimal_point();
  int length = 0;
  do {
    if((c == dec_point) && (str.find(c) != std::string::npos))
    {
      break;
    }
    str.push_back(c);
    ++length;
    c = readChar();
  } while(std::isdigit(c, loc_) || (c == dec_point));
  in_.unget();
  if (length > 20)
  {
    throw std::invalid_argument("Invalid text");
  }
  return str;
}

std::string TextReader::readStr()
{
  std::string str;
  char c = readChar();

  while (std::isspace(c,loc_))
  {
    c = readChar();
  }

  if (c == '-')
  {
    do {
      str.push_back(c);
      c = readChar();
    } while (c == '-');
    in_.unget();
    if (str.length() == 1)
    {
      str += readNum();
      last_symb_ = other;
    }
    else if (str.length() == 3)
    {
      switch (last_symb_){
      case none:
        throw std::invalid_argument("Invalid text");
        break;
      case punct:
        throw std::invalid_argument("Invalid text");
        break;
      case dash:
        throw std::invalid_argument("Invalid text");
        break;
      default:
        last_symb_ = dash;
        break;
      }
    }
    else
    {
      throw std::invalid_argument("Invalid text");
    }
  }
  else if (c == '+')
  {
    str.push_back(c);
    str += readNum();
    last_symb_ = other;
  }
  else if (std::isalpha(c,loc_))
  {
    in_.unget();
    str = readWord();
    last_symb_ = other;
  }
  else if (std::isdigit(c,loc_))
  {
    in_.unget();
    str = readNum();
    last_symb_ = other;
  }
  else if (std::ispunct(c,loc_))
  {
    switch (last_symb_)
    {
    case punct:
      throw std::invalid_argument("Invalid text");
      break;
    case none:
      throw std::invalid_argument("Invalid text");
      break;
    case comma:
      throw std::invalid_argument("Invalid text");
      break;
    case dash:
      throw std::invalid_argument("Invalid text");
      break;
    case other:
      str.push_back(c);
      if (c == ',')
      {
        last_symb_ = comma;
      }
      else
      {
        last_symb_ = punct;
      }
    }
  }
  else if (c == '\0')
  {
  
  }
  else
  {
    throw std::invalid_argument("Invalid text");
  }
  return str;
}

std::vector<std::string> TextReader::readText()
{
  std::vector<std::string> v;
  std::noskipws(in_);
  std::string str = readStr();
  while (str.size() != 0)
  {
    if ((last_symb_ == comma) || (last_symb_ == punct))
    {
      v[v.size()-1] += str;
    }
    else
    {
      v.push_back(str);
    }
    str = readStr();
  }
  return v;
}
