#include <string.h>
#include <iostream>
#include "TextReader.hpp"
#include "TextPrinter.hpp"

int main(int argc, char* argv[]) 
{
  try
  {
    unsigned int length = 0; 
    switch (argc)
    {
    case 1:
    {
      length = 40;
      break;
    }
    case 3:
    {
      if (!strcmp(argv[1],"--line-width") && std::stoi(argv[2]))
      {
        length = std::stoi(argv[2]);
      }
      else
      {
        throw std::invalid_argument("Invalid argument");
      }
      break;
    }
    default:
    {
      throw std::invalid_argument("Invalid number of arguments");
    }
    }
    if (length <= 24)
    {
      throw std::invalid_argument("Invalid length");
    }
    
    TextReader tr(std::cin);
    std::vector<std::string> vector = tr.readText();
    
    TextPrinter tp(length);
    tp.printText(vector);
  }
  catch (const std::exception& e)
  {
    std::cout << e.what() << std::endl;
    return 1;
  }
  return 0;
}
