#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>

#include "TextPrinter.hpp"


TextPrinter::TextPrinter(unsigned int length):
  length_(length)
{

}

void TextPrinter::printRange(std::vector<std::string>::const_iterator& begin, unsigned int num)
{
  for (unsigned int i = 0; i < num; ++i)
  {
    std::cout << *begin;
    begin++;
    if (i != num - 1)
    {
      std::cout << " ";
    }
  }
  std::cout << "\n";
}

void TextPrinter::printText(const std::vector<std::string> &v) 
{
  if (v.size() == 0)
  {
    return;
  }
  auto begin = v.begin();
  auto cur = begin;
  auto end = v.end();
  unsigned int width = 0;
  
  while (true)
  {
    width = 0;
    begin = cur;
    do 
    {
      width += cur->length();
      cur++;
      width++;
      if (cur == end)
      {
        break;
      }
    } while (width + cur->length() <= length_);
    if (cur == end)
    {
      printRange(begin,std::distance(begin,cur));
      break;
    }
    if (*cur == "---")
    {
      cur--;
    }
    printRange(begin,std::distance(begin,cur));
  }
}
