#ifndef TEXTPRINTER_HPP
#define TEXTPRINTER_HPP

#include <string>
#include <vector>

class TextPrinter
{
public:
  
  TextPrinter(unsigned int length);
  
  void printText(const std::vector<std::string> &v);
  
private:
  
  void printRange(std::vector<std::string>::const_iterator& begin, unsigned int num);

  const unsigned int length_;
};

#endif
