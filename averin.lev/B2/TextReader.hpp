#ifndef TEXTREADER_HPP
#define TEXTREADER_HPP

#include <string>
#include <vector>
#include <locale>

class TextReader
{
public:
  
  typedef enum
  {
    comma,punct,other,dash,none
  } last_symb_t;

  TextReader(std::istream&);

  std::vector<std::string> readText();
  
private:
  
  char readChar();
  
  std::string readStr();
  
  std::string readWord();
  
  std::string readNum();

  std::istream& in_;
  
  const std::locale loc_;
  
  last_symb_t last_symb_;
};

#endif
