#include "tasks.hpp"

void doTaskOne()
{
  std::string word;
  std::set<std::string> wordSet;
  wordSet.insert(std::istream_iterator<std::string>(std::cin), std::istream_iterator<std::string>());
  std::for_each(wordSet.begin(), wordSet.end(), [](std::string str) 
                {
                  std::cout << str << std::endl;
                });
  return;
}
