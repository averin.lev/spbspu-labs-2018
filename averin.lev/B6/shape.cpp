#include "shape.hpp"
#include <algorithm>
#include <string>
#include <iterator>
#include <stdexcept>
#include <sstream>

int getDistance(const Point &pa, const Point &pb)
{
  return ((pa.x - pb.x) * (pa.x - pb.x) + (pa.y - pb.y) * (pa.y - pb.y));
}

bool isSquare(const Shape &sh)
{
  if (sh.size() != 4)
  {
    return false;
  }
  if (getDistance(sh[0],sh[1]) == getDistance(sh[1],sh[2]))
  {
    if (getDistance(sh[0],sh[2]) == getDistance(sh[1], sh[3]) && getDistance(sh[0], sh[2]) == (getDistance(sh[0],sh[1]) + getDistance(sh[1],sh[2])))
    {
      return true;
    }
  }
  else if (getDistance(sh[0],sh[3]) == getDistance(sh[0],sh[2]))
  {
    if (getDistance(sh[0],sh[1]) == getDistance(sh[2], sh[3]) && getDistance(sh[0], sh[1]) == (getDistance(sh[0],sh[3]) + getDistance(sh[0],sh[2])))
    {
      return true;
    }
  }
  else if (getDistance(sh[0],sh[2]) == getDistance(sh[0],sh[1]))
  {
    if (getDistance(sh[2],sh[1]) == getDistance(sh[0], sh[3]) && getDistance(sh[2], sh[1]) == (getDistance(sh[0],sh[2]) + getDistance(sh[0],sh[1])))
    {
      return true;
    }
  }
  
  return false;
}

bool operator< (const Shape& lsh, const Shape& rsh)
{
  if (lsh.size() < rsh.size())
  {
    return true;
  }
  else
  {
    if ((lsh.size() == rsh.size()) && (lsh.size() == 4))
    {
      if (isSquare(lsh))
      {
        if (isSquare(rsh))
        {
          return lsh[0].x < rsh[0].x;
        }
        else
        {
          return true;
        }
      }
    }
    return false;
  }
}

std::istream& operator>> (std::istream &input, Point &p)
{
  std::string str;
  char ch = 0;
  std::getline(input, str, ')');
  if (str.empty() || input.eof())
  {
    return input;
  }
  std::istringstream stream(str);
  stream >> ch;
  if (ch != '(')
  {
    throw std::invalid_argument("Invalid point1");
  }
  stream >> p.x >> ch >> p.y;
  if (ch != ';')
  {
    throw std::invalid_argument("Invalid point2");
  }
  return input;
}
std::ostream& operator<< (std::ostream &output, const Point &p)
{
  output << " (" << p.x << ";" << p.y << ")";
  return output;
}
std::istream& operator>> (std::istream &input, Shape &sh)
{
  char ch = 0;
  size_t size = 0;
  std::string str;
  input >> size >> std::noskipws >> ch;
  if (ch == '\n')
  {
    throw std::invalid_argument("Invalid shape");
  }
  input >> std::skipws;
  std::getline(input, str);
  if (str.empty())
  {
    return input;
  }
  std::istringstream stream(str);
  Shape tmp((std::istream_iterator<Point>(stream)), std::istream_iterator<Point>());
  sh.swap(tmp);
  sh.pop_back();
  if ((size != sh.size()) || (sh.size() < 3))
  {
    throw std::invalid_argument("Invalid numbers of vertices");
  }
  return input;
}
std::ostream& operator<< (std::ostream &output, const Shape &sh)
{
  output << sh.size() << " ";
  std::copy(sh.begin(), sh.end(), std::ostream_iterator<Point>(output, " "));
  output << std::endl;
  return output;
}
