#include "tasks.hpp"
#include <vector>
#include "shape.hpp"

void doTaskTwo()
{
  size_t vertices = 0;
  size_t squares = 0;
  size_t rectangles = 0;
  size_t triangles = 0;
  if (!std::cin)
  {
    throw std::invalid_argument("Invalid input");
  }
  std::cin >> std::skipws;
  std::vector<Shape> shapes(std::istream_iterator<Shape>(std::cin), std::istream_iterator<Shape>());
  shapes.erase(std::remove_if(shapes.begin(), shapes.end(), [&vertices](const Shape &sh)
                 {
                   vertices += sh.size();
                   return ((sh.size() == 5) || (sh.size() == 0));
                 }), shapes.end ());
  std::vector<Point> points;
  std::for_each(shapes.begin(), shapes.end(), [&points, &triangles, &rectangles, &squares](const Shape &shape)
                {
                  points.push_back(shape.front());
                  if (shape.size() == 3)
                  {
                    triangles++;
                  }
                  if (shape.size() == 4)
                  {
                    rectangles++;
                    if (isSquare(shape))
                    {
                      squares++;
                    }
                  }
                });
  std::sort(shapes.begin(), shapes.end());
  std::cout << "Vertices: " << vertices << std::endl;
  std::cout << "Triangles: " << triangles << std::endl;
  std::cout << "Squares: " << squares << std::endl;
  std::cout << "Rectangles: " << rectangles << std::endl;
  std::cout << "Points:";
  std::copy (points.begin(), points.end(), std::ostream_iterator<Point>(std::cout));
  std::cout << std::endl << "Shapes:" << std::endl;
  std::copy (shapes.begin(), shapes.end(), std::ostream_iterator<Shape>(std::cout));
}
