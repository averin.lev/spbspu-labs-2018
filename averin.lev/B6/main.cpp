#include <iostream>
#include <string>
#include <stdexcept>
#include "tasks.hpp"

int main(int argc, char* argv[])
{
  try
  {
    if (argc < 2)
    {
      throw std::invalid_argument("Invalid number of arguments");
    }
    switch (std::stoi(argv[1]))
    {
      case 1:
        doTaskOne();
        break;
      case 2:
        doTaskTwo();
        break;
      default:
        throw std::invalid_argument("Invalid argument");
    }
  }
  catch (std::invalid_argument& e)
  {
    std::cout << e.what() << std::endl;
    return 1;
  }
  return 0;  
}
