#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <vector>
#include <iostream>

struct Point
{
  int x, y;
};

using Shape = std::vector<Point>;
std::istream& operator>> (std::istream &, Point &p);
std::ostream& operator<< (std::ostream &, const Point &p);
std::istream& operator>> (std::istream &, Shape &sh);
std::ostream& operator<< (std::ostream &, const Shape &sh);
bool operator< (const Shape& lsh, const Shape& rhs);
bool isSquare(const Shape &sh);

#endif
