#ifndef PARTS_HPP
#define PARTS_HPP
#include <iostream>
#include <stdexcept>
#include <set>
#include <algorithm>
#include <iterator>

void doTaskOne();
void doTaskTwo();

#endif
